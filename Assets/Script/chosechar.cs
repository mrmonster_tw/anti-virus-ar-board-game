﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chosechar : MonoBehaviour
{
    public float len;
    public GameObject char_ob;
    public GameObject gm;

    // Start is called before the first frame update
    void Start()
    {
        len = GetComponent<AudioSource>().clip.length;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.tag == "skillchar")
        {
            if (len < 0)
            {
                gm.GetComponent<GM>().player_char_talking = false;
                char_ob.GetComponent<Animator>().Play("skillchar_close");
                this.GetComponent<Animator>().Play("idle");

                gm.GetComponent<GM>().attitude.SetActive(true);
                gm.GetComponent<GM>().knowl.SetActive(true);
                gm.GetComponent<GM>().skill.SetActive(true);
                gm.GetComponent<GM>().target.SetActive(true);
                gm.GetComponent<GM>().chart.SetActive(true);
                len = 0;
            }
            if (len > 0)
            {
                gm.GetComponent<GM>().player_char_talking = true;
                len = len - 1f * Time.deltaTime;
            }
        }
        if (this.tag == "win")
        {
            if (len == 0)
            {
                len = GetComponent<AudioSource>().clip.length;
            }
            
            if (len < 0)
            {
                gm.GetComponent<GM>().player_char_talking = false;
                gm.GetComponent<GM>().attitude.SetActive(true);
                gm.GetComponent<GM>().knowl.SetActive(true);
                gm.GetComponent<GM>().skill.SetActive(true);
                gm.GetComponent<GM>().target.SetActive(true);
                gm.GetComponent<GM>().chart.SetActive(true);
                gm.GetComponent<GM>().once_1 = 0;
                len = 0;
                this.gameObject.SetActive(false);
            }
            if (len > 0)
            {
                gm.GetComponent<GM>().player_char_talking = true;
                len = len - 1f * Time.deltaTime;
            }
        }
        if (this.tag == "destroy")
        {
            if (len == 0)
            {
                len = GetComponent<AudioSource>().clip.length;
            }

            if (len < 0)
            {
                //gm.GetComponent<GM>().player_char_talking = false;
                //gm.GetComponent<GM>().attitude.SetActive(true);
                //gm.GetComponent<GM>().knowl.SetActive(true);
                //gm.GetComponent<GM>().skill.SetActive(true);
                //gm.GetComponent<GM>().target.SetActive(true);
                //gm.GetComponent<GM>().chart.SetActive(true);
                gm.GetComponent<GM>().once_1 = 0;
                len = 0;
                gm.GetComponent<GM>().cancal.transform.localPosition = new Vector3(450, 850, 0);
                gm.GetComponent<GM>().cancal.GetComponent<Animator>().enabled = true;
                //this.gameObject.SetActive(false);
                //gm.GetComponent<GM>().destroy_button.transform.localPosition = new Vector3(1500, 0, 0);
            }
            if (len > 0)
            {
                gm.GetComponent<GM>().player_char_talking = true;
                len = len - 1f * Time.deltaTime;
            }
        }
    }
}
