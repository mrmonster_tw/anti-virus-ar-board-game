﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;
using TMPro;

public class EasyImageTargetBehaviour : ImageTargetBehaviour
{
    private Dictionary<string, bool> _inerlist = null;
    private GameObject scan = null, loading = null;
    private bool _getTarget;
    private GM _gm;

    protected override void Awake()
    {
        base.Awake();
        _gm = GameObject.Find("Canvas").GetComponent<GM>();
        _inerlist = GameObject.Find("Canvas").GetComponent<GM>().getcomponentlist();
        scan = GameObject.Find("Canvas").GetComponent<GM>().scan;
        loading = GameObject.Find("Canvas").GetComponent<GM>().loading_ui;
        _getTarget = GameObject.Find("Canvas").GetComponent<GM>().getTarget;

        int total = _inerlist.Count;
        loading.SetActive(true);
        scan.SetActive(false);
        
        TargetFound += OnTargetFound;
        TargetLost += OnTargetLost;
        TargetLoad += OnTargetLoad;
        TargetUnload += OnTargetUnload;
    }
    protected override void Start()
    {
        base.Start();
        HideObjects(transform);
    }
    void HideObjects(Transform trans)
    {
        for (int i = 0; i < trans.childCount; ++i) HideObjects(trans.GetChild(i));
        if (transform != trans) gameObject.SetActive(false);
    }
    void ShowObjects(Transform trans)
    {
        for (int i = 0; i < trans.childCount; ++i) ShowObjects(trans.GetChild(i));
        if (transform != trans) gameObject.SetActive(true);
    }
    void OnTargetFound(TargetAbstractBehaviour behaviour)
    {
        if (!_getTarget && !scan.activeSelf)
        {
            HideObjects(transform);
        }
        _getTarget = true;
        //Debug.Log("Found: " + Target.Name);       
    }
    void OnTargetLost(TargetAbstractBehaviour behaviour)
    {
        HideObjects(transform);
        _getTarget = false;
        //Debug.Log("Lost: " + Target.Name);
    }
    void OnTargetLoad(ImageTargetBaseBehaviour behaviour, ImageTrackerBaseBehaviour tracker, bool status)
    {
        int total = _inerlist.Count;

        _inerlist[Target.Name] = true;
        
        //Debug.Log("Load target (" + status + "): " + Target.Id + " (" + Target.Name + ") " + " -> " + tracker);

        int completenum = 0;
        foreach(bool a in _inerlist.Values)
        {            
            if (a) completenum++;
        }

        if(completenum == total)
        {
            //Debug.Log("loading completed");
            loading.SetActive(false);
            scan.SetActive(true);
        }
        else
        {
            loading.transform.Find("bak").transform.Find("text").GetComponent<TextMeshProUGUI>().text
                = ("\n(" + completenum + "/" + total+")");
            //Debug.Log("still loading objects:"+completenum+"/"+ total);
        }
    }
    void OnTargetUnload(ImageTargetBaseBehaviour behaviour, ImageTrackerBaseBehaviour tracker, bool status)
    {
        //Debug.Log("Unload target (" + status + "): " + Target.Id + " (" + Target.Name + ") " + " -> " + tracker);
    }
}

