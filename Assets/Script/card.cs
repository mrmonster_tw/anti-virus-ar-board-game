﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class card : MonoBehaviour {

	public bool tar;
    public bool skill;

    public GameObject opt;
	public GameObject gm;
    public GameObject cam;

    //	int no_player_hint_check = 0;
    public int check;
    public int check_1 = 0;
    public int check_2 = 0;

    public float len;
    public GameObject char_ob;

    public bool anwsered;
    public int score;
    public string right_ans;

    // Use this for initialization
    void Start () 
	{
        len = GetComponent<AudioSource>().clip.length;
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (this.GetComponent<AudioSource>().enabled && len > 0 && skill)
        {
            len = len - 1f * Time.deltaTime;
            if (check_1 == 0)
            {
                char_ob.GetComponent<Animator>().Play("skill_char");

                check_1 = 1;
            }
        }
            
        if (len < 0)
        {
            char_ob.GetComponent<Animator>().Play("skillchar_close");
            len = 0;
        }
		if (gm.GetComponent<GM> ().hold == false)
		{
			if (gm.GetComponent<GM> ().target_ob != null && gm.GetComponent<GM> ().target_ob.name == this.name+"_ob")
			{
				tar = true;
			}
			else
			{
				tar = false;
			}

			if (tar && gm.GetComponent<GM> ().player_char != null && gm.GetComponent<GM> ().hold == false && gm.GetComponent<GM> ().target_ui == false)
			{
				if (this.GetComponent<card>().anwsered && this.GetComponent<card>().check == 0)
				{
                    gm.GetComponent<GM>().target_ui = this.gameObject;
					this.transform.localPosition = new Vector3 (1500,-100,0);
					gm.GetComponent<GM> ().answered_ui.transform.localPosition = new Vector3 (0, -100, 0);
					gm.GetComponent<GM> ().cancal.transform.localPosition = new Vector3 (450, 850, 0);
					gm.GetComponent<GM> ().answered_ui_text.text = this.GetComponent<card> ().right_ans;
					this.GetComponent<AudioSource> ().enabled = false;
                    if (check_2 == 1)
                    {
                        gm.GetComponent<GM>().answered_ui_1.transform.localPosition = new Vector3(0, 0, 0);
                        gm.GetComponent<GM>().cancal.GetComponent<Animator>().enabled = true;
                    }
                    check = 1;
                }
                
                else if (this.GetComponent<card>().anwsered == false && this.GetComponent<card>().check == 0)
                {
                    gm.GetComponent<GM>().target_ui = this.gameObject;
                    this.transform.localPosition = new Vector3(0, -100, 0);
                    this.GetComponent<AudioSource>().enabled = true;
                    gm.GetComponent<GM>().cancal.transform.localPosition = new Vector3(450, 850, 0);

                    GameObject opt_cn = Instantiate(opt, cam.transform);
                    opt_cn.transform.localPosition = new Vector3(0, 0, 0.3f);
                    opt_cn.transform.localEulerAngles = new Vector3(270, 0, 0);
                    gm.GetComponent<GM>().opt = opt_cn;

                    check = 1;
                }
			}
			else
			{
				//this.GetComponent<AudioSource> ().enabled = false;
			}
		}
		else
		{
			this.transform.localPosition = new Vector3 (1500,-100,0);
			this.GetComponent<AudioSource> ().enabled = false;
            len = GetComponent<AudioSource>().clip.length;
            check_1 = 0;
        }
	}
}
