﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GM : MonoBehaviour {

	public GameObject target_ob;
	public GameObject target_ui;
	public GameObject yes;
	public GameObject no;
	public GameObject check;
	public int score;
	public int score_10;
	public int score_1;
	public GameObject score_ui;
	public Text score_ui_text;
	public GameObject cancal;

	public GameObject player_char_temp;
	public GameObject player_char;
    public bool player_char_talking;

    //	public GameObject choosed;

    public GameObject target_card_1;
	public GameObject target_card_2;
	public GameObject target_card_3;
	public bool gameover;
	public int gameover_check;
	public bool hold;
	public GameObject gameover_ui;
	public GameObject answered_ui;
    public GameObject answered_ui_1;
    public Text answered_ui_text;
	public GameObject setting_ui;
    public GameObject loading_ui;
	public GameObject no_player_hint;
	public GameObject scan;
	public AudioClip click_aud;

	public RawImage star_1;
	public RawImage star_2;
	public RawImage star_3;
	public RawImage star_4;
	public RawImage star_5;
	public Texture star_full;
	public Texture star_ept;
	public RawImage rawnum_1;
	public RawImage rawnum_2;
	public Texture num_0;
	public Texture num_1;
	public Texture num_2;
	public Texture num_3;
	public Texture num_4;
	public Texture num_5;
	public Texture num_6;
	public Texture num_7;
	public Texture num_8;
	public Texture num_9;

	public GameObject all_ui;
	public GameObject Q_knowl_1_ui;
	public GameObject Q_knowl_2_ui;
	public GameObject Q_knowl_3_ui;
	public GameObject Q_knowl_4_ui;
	public GameObject Q_knowl_5_ui;
	public GameObject Q_knowl_6_ui;
	public GameObject Q_knowl_7_ui;
	public GameObject Q_knowl_8_ui;
	public GameObject Q_knowl_9_ui;
	public GameObject Q_knowl_10_ui;

	public GameObject Q_attitude_1_ui;
	public GameObject Q_attitude_2_ui;
	public GameObject Q_attitude_3_ui;
	public GameObject Q_attitude_4_ui;
	public GameObject Q_attitude_5_ui;
	public GameObject Q_attitude_6_ui;
	public GameObject Q_attitude_7_ui;
	public GameObject Q_attitude_8_ui;
	public GameObject Q_attitude_9_ui;
	public GameObject Q_attitude_10_ui;

	public GameObject Q_skill_1_ui;
	public GameObject Q_skill_2_ui;
	public GameObject Q_skill_3_ui;
	public GameObject Q_skill_4_ui;
	public GameObject Q_skill_5_ui;
	public GameObject Q_skill_6_ui;
	public GameObject Q_skill_7_ui;
	public GameObject Q_skill_8_ui;
	public GameObject Q_skill_9_ui;
	public GameObject Q_skill_10_ui;

    public Texture Elf_1_pic;
	public Texture Elf_2_pic;
	public Texture Elf_3_pic;
	public Texture Elf_4_pic;
	public Texture Elf_5_pic;
	public Texture Wizard_1_pic;
	public Texture Wizard_2_pic;
	public Texture Wizard_3_pic;
	public Texture Wizard_4_pic;
	public Texture Wizard_5_pic;
	public GameObject char_main;

    public GameObject Elf_1;
    public GameObject Elf_2;
    public GameObject Elf_3;
    public GameObject Elf_4;
    public GameObject Elf_5;
    public GameObject Wizard_1;
    public GameObject Wizard_2;
    public GameObject Wizard_3;
    public GameObject Wizard_4;
    public GameObject Wizard_5;
    public GameObject gummy;
    public GameObject jelly;
    public GameObject coffce;


    public GameObject chart;
    public GameObject attitude;
    public GameObject knowl;
    public GameObject skill;
    public GameObject target;
    public int once;
    public int once_1;


    public GameObject shinyitem;
    public GameObject shinyitem_1;
    public GameObject smoke;
    public GameObject skull_blow;
	public GameObject go_hint;
	public GameObject destroy_button;
    public GameObject opt;
    //	public GameObject fpage;

    public GameObject skill_char;
    public bool skill_qs;
    public bool getTarget = false;

    string[] comlist = new string[] { "Elf_1", "Elf_2", "Elf_3", "Elf_4", "Elf_5",
        "Wizard_1","Wizard_2","Wizard_3","Wizard_4","Wizard_5",
        "Goal_candy","Goal_jelly","Goal_coffee",
        "Q_skill_1","Q_skill_2","Q_skill_3","Q_skill_4","Q_skill_5","Q_skill_6","Q_skill_7",
        "Q_skill_8","Q_skill_9","Q_skill_10",
        "Q_knowl_1","Q_knowl_2","Q_knowl_3","Q_knowl_4","Q_knowl_5","Q_knowl_6","Q_knowl_7",
        "Q_knowl_8","Q_knowl_9","Q_knowl_10",
        "Q_attitude_1","Q_attitude_2","Q_attitude_3","Q_attitude_4","Q_attitude_5","Q_attitude_6",
        "Q_attitude_7","Q_attitude_8","Q_attitude_9","Q_attitude_10"
    };
    private Dictionary<string, bool> ComponentList = new Dictionary<string, bool>();

    // Use this for initialization
    void Start () 
	{
        for (int i = 0; i < comlist.Length; i++)
        {
            ComponentList.Add(comlist[i], false);
        }
        loading_ui.transform.Find("bak").transform.Find("text").GetComponent<TextMeshProUGUI>().text
= ("\n(0/" + ComponentList.Count + ")");
    }
	public Dictionary<string, bool> getcomponentlist()
    {
        return ComponentList;
    }

	// Update is called once per frame
	void Update () 
	{
        target_ob = GameObject.FindGameObjectWithTag ("card");
		score_ui_text.text = score.ToString ();

        if (player_char != null)
        {
            if (gameover)
            {
                //				if (Input.GetMouseButtonDown (0) || (Input.touchCount >0 && Input.GetTouch(0).phase == TouchPhase.Began))
                //				{
                //					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
                //					RaycastHit hit;
                //					Physics.Raycast (ray, out hit);
                //					if (hit.transform.gameObject.tag == "card") 
                //					{
                //						
                //					} 
                //				}
                if (gameover_check == 0)
                {
                    //if (skill_qs)
                    //{
                    //skill_char.GetComponent<Animator>().Play("skillchar_close");
                    //}
                    //destroy_button.transform.localPosition = new Vector3 (0,0,0);
                    all_ui.SetActive(false);
                    score_ui.SetActive(false);
                    answered_ui.SetActive(false);
                    cancal.SetActive(false);
                    score_10 = score / 10;
                    score_1 = score - (score_10 * 10);
                    numraw_fn(score_10, rawnum_1);
                    numraw_fn(score_1, rawnum_2);
                    star_fn();
                    gameover_check = 1;
                }
                /*if (target_ob != null && target_ob.name != "drag")
                {
                    go_hint.transform.localPosition = new Vector3(0, 0, 0);
                    destroy_button.transform.localPosition = new Vector3(1500, 0, 0);
                }
                if (target_ob != null && target_ob.name == "drag")
                {
                    destroy_button.transform.localPosition = new Vector3(0, 0, 0);
                }
                if (target_ob == null)
                {
                    go_hint.transform.localPosition = new Vector3(1500, 0, 0);
                    destroy_button.transform.localPosition = new Vector3(1500, 0, 0);
                }*/
            }
            else if (gameover == false)
            {
                skill_char_fn("Q_skill_1_ob");
                skill_char_fn("Q_skill_2_ob");
                skill_char_fn("Q_skill_3_ob");
                skill_char_fn("Q_skill_4_ob");
                skill_char_fn("Q_skill_5_ob");
                skill_char_fn("Q_skill_6_ob");
                skill_char_fn("Q_skill_7_ob");
                skill_char_fn("Q_skill_8_ob");
                skill_char_fn("Q_skill_9_ob");
                skill_char_fn("Q_skill_10_ob");
                if (player_char_talking == false && target_ui == null)
                {
                    rechose_char_fn("Elf_1");
                    rechose_char_fn("Elf_2");
                    rechose_char_fn("Elf_3");
                    rechose_char_fn("Elf_4");
                    rechose_char_fn("Elf_5");
                    rechose_char_fn("Wizard_1");
                    rechose_char_fn("Wizard_2");
                    rechose_char_fn("Wizard_3");
                    rechose_char_fn("Wizard_4");
                    rechose_char_fn("Wizard_5");

                    if (target_card_1.activeSelf && once_1 == 0)
                    {
                        gummy.SetActive(true);
                        chart.SetActive(false);
                        attitude.SetActive(false);
                        knowl.SetActive(false);
                        skill.SetActive(false);
                        target.SetActive(false);
                        once_1 = 1;
                    }
                    if (target_card_2.activeSelf && once_1 == 0)
                    {
                        jelly.SetActive(true);
                        chart.SetActive(false);
                        attitude.SetActive(false);
                        knowl.SetActive(false);
                        skill.SetActive(false);
                        target.SetActive(false);
                        once_1 = 1;
                    }
                    if (target_card_3.activeSelf && once_1 == 0)
                    {
                        destroy_button.transform.localPosition = new Vector3(0, 0, 0);
                        coffce.SetActive(true);
                        chart.SetActive(false);
                        attitude.SetActive(false);
                        knowl.SetActive(false);
                        skill.SetActive(false);
                        target.SetActive(false);
                        once_1 = 1;
                    }
                    
                }
               
                

                if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);

                    if (target_ui != null && target_ui.GetComponent<card>().anwsered == false)
                    {
                        if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                        {
                            print("UI");
                        }
                        //if (EventSystem.current.IsPointerOverGameObject ())
                        //{
                            //print ("UI");
                        //} 
                        else
                        {
                            if (hit.transform.gameObject.name == target_ui.GetComponent<card>().right_ans)
                            {
                                print("yes");
                                cancal.GetComponent<Animator>().enabled = true;
                                target_ui.transform.localPosition = new Vector3(1500, -100, 0);
                                score_fn();
                                yes.SetActive(true);
                                hit.transform.gameObject.GetComponent<rotate>().check = true;
                                answered_ui.transform.localPosition = new Vector3(0, -100, 0);
                                answered_ui_text.text = target_ui.GetComponent<card>().right_ans;
                                target_ui.GetComponent<AudioSource>().enabled = false;
                                //hit.transform.gameObject.GetComponent<rotate>().check = true;
                                //hit.transform.gameObject.GetComponent<rotate> ().enabled = true;
                                if (skill_char.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("skill_char"))
                                {
                                    skill_char.GetComponent<Animator>().Play("skillchar_close");
                                }

                                StartCoroutine(wait_close(hit.transform.gameObject));
                                target_ui.GetComponent<card>().anwsered = true;
                            }
                            else
                            {
                                print("no");
                                cancal.GetComponent<Animator>().enabled = true;
                                target_ui.transform.localPosition = new Vector3(1500, -100, 0);
                                no.SetActive(true);
                                hit.transform.gameObject.GetComponent<rotate>().check = true;
                                answered_ui.transform.localPosition = new Vector3(0, -100, 0);
                                answered_ui_text.text = target_ui.GetComponent<card>().right_ans;
                                target_ui.GetComponent<AudioSource>().enabled = false;
                                //hit.transform.gameObject.GetComponent<rotate>().check = true;
                                //hit.transform.gameObject.GetComponent<rotate> ().enabled = true;
                                if (skill_char.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("skill_char"))
                                {
                                    skill_char.GetComponent<Animator>().Play("skillchar_close");
                                }

                                StartCoroutine(wait_close(hit.transform.gameObject));
                                target_ui.GetComponent<card>().anwsered = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            player_fn("Elf_1");
            player_fn("Elf_2");
            player_fn("Elf_3");
            player_fn("Elf_4");
            player_fn("Elf_5");
            player_fn("Wizard_1");
            player_fn("Wizard_2");
            player_fn("Wizard_3");
            player_fn("Wizard_4");
            player_fn("Wizard_5");

            if (player_char == null && player_char_temp == null)
            {
                check.SetActive(false);
            }
            if (target_ob == null)
            {
                player_char_temp = null;
            }
            if (target_ob != null && player_char == null && player_char_temp == null)
            {
                no_player_hint.transform.localPosition = new Vector3(0, 0, 0);
            }
        }
        //掃描框
        if (target_ob != null || player_char_talking)
        {
            scan.GetComponent<Animator>().Play("room_out");
        }
        if (target_ob == null && target_ui == null && player_char_talking == false)
        {
            print("aa");
            scan.GetComponent<Animator>().Play("room_in");
            //check.SetActive (false);
            no_player_hint.transform.localPosition = new Vector3(1500, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            setting_ui.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
	private IEnumerator wait_close (GameObject xx) 
	{
		yield return new WaitForSeconds (2f);
		yes.SetActive (false);
		no.SetActive (false);

        //生閃光
        GameObject shinyitem_cn = Instantiate(shinyitem, xx.transform);
        GameObject shinyitem_1_cn = Instantiate(shinyitem_1, xx.transform);
        shinyitem_cn.transform.localPosition = new Vector3(0, 15, 0);
        shinyitem_1_cn.transform.localPosition = new Vector3(0, 0, 0);
        shinyitem_cn.transform.localScale = new Vector3(50, 50, 50);
        shinyitem_1_cn.transform.localScale = new Vector3(50, 50, 50);
    }
    public void close_fn()
    {
        Destroy(opt);
        destroy_button.transform.localPosition = new Vector3(1500, 0, 0);
        coffce.SetActive(false);
        player_char_talking = false;
        if (target_ui != null)
        {
            target_ui.GetComponent<card>().check = 0;
            target_ui.GetComponent<card>().check_2 = 1;
            target_ui.GetComponent<AudioSource>().enabled = false;
        }
        
        cancal.GetComponent<Animator>().enabled = false;
        cancal.transform.localScale = new Vector3(1,1,1);
        hold = true;
        StartCoroutine(hold_wait());
        cancal.transform.localPosition = new Vector3(1500, 850, 0);
        answered_ui.transform.localPosition = new Vector3(1500, -100, 0);
        answered_ui_1.transform.localPosition = new Vector3(1500, -100, 0);
        target_ui = null;
        if (skill_qs)
        {
            skill_char.GetComponent<Animator>().Play("skillchar_close");
        }
        skill_qs = false;

        attitude.SetActive(true);
        knowl.SetActive(true);
        skill.SetActive(true);
        target.SetActive(true);
        chart.SetActive(true);
    }
    public void skill_char_fn (string xx)
	{
        if (target_ob != null && target_ob.name == xx)
        {
            skill_qs = true;
            Elf_1.GetComponent<Animator>().Play("talk");
            Elf_2.GetComponent<Animator>().Play("talk");
            Elf_3.GetComponent<Animator>().Play("talk");
            Elf_4.GetComponent<Animator>().Play("talk");
            Elf_5.GetComponent<Animator>().Play("talk");
            Wizard_1.GetComponent<Animator>().Play("talk");
            Wizard_2.GetComponent<Animator>().Play("talk");
            Wizard_3.GetComponent<Animator>().Play("talk");
            Wizard_4.GetComponent<Animator>().Play("talk");
            Wizard_5.GetComponent<Animator>().Play("talk");
        }
    }
    bool sseck;
    Transform[] tartr;
    public void rechose_char_fn(string xx)
    {
        if (target_ob != null && target_ob.name == xx  && once == 1)
        {
            skill_char.GetComponent<Animator>().Play("skill_char");
            sseck = true;
            tartr = target_ob.GetComponentsInChildren<Transform>();
            tartr[1].position = new Vector3(10000, 0, 0);

            Elf_1.GetComponent<Animator>().Play("idle");
            Elf_2.GetComponent<Animator>().Play("idle");
            Elf_3.GetComponent<Animator>().Play("idle");
            Elf_4.GetComponent<Animator>().Play("idle");
            Elf_5.GetComponent<Animator>().Play("idle");
            Wizard_1.GetComponent<Animator>().Play("idle");
            Wizard_2.GetComponent<Animator>().Play("idle");
            Wizard_3.GetComponent<Animator>().Play("idle");
            Wizard_4.GetComponent<Animator>().Play("idle");
            Wizard_5.GetComponent<Animator>().Play("idle");
        }
        if (target_ob == null && sseck)
        {
            if (skill_char.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("skill_char"))
            {
                skill_char.GetComponent<Animator>().Play("skillchar_close");
            }
            sseck = false;
            chart.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    public void char_sure_fn ()
	{
        chart.SetActive(false);
        attitude.SetActive(false);
        knowl.SetActive(false);
        skill.SetActive(false);
        target.SetActive(false);

        player_char = player_char_temp;
		score_ui.SetActive (true);
		check.SetActive (false);

        char_show_fn("Elf_1", Elf_1, Elf_1_pic, elf);
        char_show_fn("Elf_2", Elf_2, Elf_2_pic, elf);
        char_show_fn("Elf_3", Elf_3, Elf_3_pic, elf);
        char_show_fn("Elf_4", Elf_4, Elf_4_pic, elf);
        char_show_fn("Elf_5", Elf_5, Elf_5_pic, elf);
        char_show_fn("Wizard_1", Wizard_1, Wizard_1_pic, wizard);
        char_show_fn("Wizard_2", Wizard_2, Wizard_2_pic, wizard);
        char_show_fn("Wizard_3", Wizard_3, Wizard_3_pic, wizard);
        char_show_fn("Wizard_4", Wizard_4, Wizard_4_pic, wizard);
        char_show_fn("Wizard_5", Wizard_5, Wizard_5_pic, wizard);
        once = 1;
    }
    public AudioClip wizard;
    public AudioClip elf;

    public void char_show_fn(string zz, GameObject xx , Texture yy, AudioClip rr)
    {
        if (player_char.name == zz)
        {
            gameover_ui.GetComponent<AudioSource>().clip = rr;
            xx.SetActive(true);
            char_main.GetComponent<RawImage>().texture = yy;
            xx.GetComponentInChildren<Animator>().Play("introduce");
            xx.GetComponent<AudioSource>().Play();
        }
    }
    public void score_fn ()
	{
		score = score + target_ui.GetComponent<card> ().score;
	}
	public void restart_fn ()
	{
		SceneManager.LoadScene (0);
	}
	private IEnumerator hold_wait () 
	{
		yield return new WaitForSeconds (1f);
		hold = false;
	}
	public void numraw_fn (int xx , RawImage yy)
	{
		if (xx == 0)
		{
			yy.texture = num_0;
		}
		if (xx == 1)
		{
			yy.texture = num_1;
		}
		if (xx == 2)
		{
			yy.texture = num_2;
		}
		if (xx == 3)
		{
			yy.texture = num_3;
		}
		if (xx == 4)
		{
			yy.texture = num_4;
		}
		if (xx == 5)
		{
			yy.texture = num_5;
		}
		if (xx == 6)
		{
			yy.texture = num_6;
		}
		if (xx == 7)
		{
			yy.texture = num_7;
		}
		if (xx == 8)
		{
			yy.texture = num_8;
		}
		if (xx == 9)
		{
			yy.texture = num_9;
		}
	}
	public void star_fn ()
	{
		if (score > 25)
		{
			star_1.texture = star_full;
			star_2.texture = star_full;
			star_3.texture = star_full;
			star_4.texture = star_full;
			star_5.texture = star_full;
		}
		if (score > 20 && score <= 25)
		{
			star_1.texture = star_full;
			star_2.texture = star_full;
			star_3.texture = star_full;
			star_4.texture = star_full;
		}
		if (score > 10 && score <= 20)
		{
			star_1.texture = star_full;
			star_2.texture = star_full;
			star_3.texture = star_full;
		}
		if (score > 5 && score <= 10)
		{
			star_1.texture = star_full;
			star_2.texture = star_full;
		}
		if (score > 0 && score <= 5)
		{
			star_1.texture = star_full;
		}
	}
	private IEnumerator star_fn_wait () 
	{
		yield return new WaitForSeconds (0.5f);
		star_1.texture = star_full;
	}
	public void Quit_fn ()
	{
		Application.Quit();
	}
	public void Quitcan_fn ()
	{
		setting_ui.transform.localPosition = new Vector3 (1500,0,0);
	}
	public void destroy_drag ()
	{
        gameover = true;

        go_hint.SetActive (false);
		destroy_button.SetActive (false);
        coffce.SetActive(false);
        //Destroy (target_ob);
        //生爆炸
        GameObject skull_blow_cn = Instantiate (skull_blow, skill_char.transform);
        //skull_blow_cn.transform.localPosition = new Vector3 (0,20, 0);
        //skull_blow_cn.transform.localScale = new Vector3 (80, 80, 80);
        skull_blow_cn.transform.localPosition = new Vector3(-7.5f,0,0);
        skull_blow_cn.transform.eulerAngles = new Vector3(130, 0, 0);
        skull_blow_cn.transform.localScale = new Vector3(0.8f , 0.8f, 0.8f);
        Destroy (skull_blow_cn,3f);
        scan.SetActive(false);
        StartCoroutine (score_fn_wait ());
	}
	public void click_aud_fn ()
	{
		this.GetComponent<AudioSource> ().Play ();
	}

    Animator[] sss;

    private IEnumerator score_fn_wait () 
	{
		yield return new WaitForSeconds (3f);
        skill_char.GetComponent<Animator>().Play("skill_char");
        sss = skill_char.GetComponentsInChildren<Animator>();
        sss[1].Play("epilogue");
        gameover_ui.GetComponent<AudioSource>().enabled = true;
        yield return new WaitForSeconds(19f);
        GameObject smoke_cn = Instantiate(smoke, skill_char.transform);
        smoke_cn.transform.localPosition = new Vector3(0, 0.5f, 0);
        smoke_cn.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        yield return new WaitForSeconds(1f);
        skill_char.SetActive(false);
        gameover_ui.transform.localPosition = new Vector3 (0,0,0);
	}
	public void player_fn (string xx)
	{
		if (target_ob != null && target_ob.name == xx)
		{
			player_char_temp = target_ob;
			check.SetActive (true);
		}
    }
	/*private IEnumerator fpage_fn () 
	{
		yield return new WaitForSeconds (4f);
		fpage.GetComponent<Animator> ().enabled = true;
		yield return new WaitForSeconds (1.5f);
		Destroy (fpage);
	}*/
}
