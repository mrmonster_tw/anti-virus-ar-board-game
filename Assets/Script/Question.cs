﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question : MonoBehaviour {

	public bool anwsered;
	public GameObject card;
	public GameObject yes;
	public GameObject no;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown(0) && anwsered == false)
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);//从相机向鼠标点击点发射条射线
			RaycastHit hit;

			Physics.Raycast (ray, out hit);

			if (hit.transform.gameObject.tag == "right") 
			{
				yes.SetActive (true);
				hit.transform.gameObject.GetComponent<Animator> ().Play("box_chose");
				Destroy (hit.transform.gameObject,2f);
				anwsered = true;
				StartCoroutine (wait_close ());
			} 
			else 
			{
				no.SetActive (true);
				hit.transform.gameObject.GetComponent<Animator> ().Play("box_chose");
				Destroy (hit.transform.gameObject,2f);
				anwsered = true;
				StartCoroutine (wait_close ());
			}

		}

		if (card.activeSelf) 
		{
			this.transform.localPosition = new Vector3 (0, 0, 0);
		} 
		else 
		{
			this.transform.localPosition = new Vector3 (1500, 0, 0);
		}
	}
	private IEnumerator wait_close () 
	{
		yield return new WaitForSeconds (2f);
		yes.SetActive (false);
		no.SetActive (false);
	}
}
